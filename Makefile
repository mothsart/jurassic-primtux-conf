.PHONY: build.debian build.debian.source

none:
	echo ""

build.debian:
	debuild -us -uc #binary package : .deb, alias of dpkg-buildpackage -rfakeroot -d -us -uc

build.debian.source:
	debuild -S #source package : alias of dpkg-buildpackage -rfakeroot -d -us -uc -S
